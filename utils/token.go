package utils

import (
	"encoding/json"
	"net/http"

	"github.com/gocolly/colly/v2"
)

// IsValid checks the validity of the existing token
func IsValid(token string) bool {
	user := struct {
		CustomerTitle string `json:"customerTitle"`
		Username      string `json:"mfdOnlineUserName"`
		Email         string `json:"email"`
	}{}
	hdr := http.Header{}
	hdr.Add("Host", "d11.emofid.com")
	hdr.Add("User-Agent", userAgent)
	hdr.Add("Accept", "application/json, text/plain, */*")
	hdr.Add("Sec-Fetch-Dest", "empty")
	hdr.Add("Authorization", "Bearer "+token)
	hdr.Add("Origin", "https://d.easytrader.emofid.com")
	hdr.Add("Sec-Fetch-Site", "same-site")
	hdr.Add("Sec-Fetch-Mode", "cors")
	hdr.Add("Referer", "https://d.easytrader.emofid.com/auth-callback")
	test := CustomCollector()
	test.OnResponse(func(r *colly.Response) {
		_ = json.Unmarshal(r.Body, &user)
	})
	test.Request("GET", "https://d11.emofid.com/easy/api/account/checkuser", nil, nil, hdr)

	return user.Username != ""
}
