package utils

import (
	"github.com/gocolly/colly/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"

func CustomCollector() *colly.Collector {
	c := colly.NewCollector()
	c.UserAgent = userAgent
	c.AllowURLRevisit = true
	if viper.GetBool("debug") {
		logrus.Debug("setting proxy")
		if err := c.SetProxy("http://127.0.0.1:8080"); err != nil {
			logrus.Fatalf("failed to set proxy: %v", err)
		}
	}
	return c
}
