package cmd

import (
	"encoding/json"
	"time"

	"bitbucket.org/MShoaei/Stock/miner/utils"
	"github.com/gocolly/colly/v2"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type side int

const (
	buy  side = 0
	sell side = 1
)

var orderSide int
var orderISIN string
var orderPrice uint
var orderQuantity uint
var orderISINSlice []string
var orderPriceSlice []uint
var orderQuantitySlice []uint
var orderValidityDate string
var orderAt string
var orderAggressive bool

// orderCmd represents the order command
var orderCmd = &cobra.Command{
	Use:   "order",
	Short: "low level command for placing orders",
	Long: `low-level command for placing orders.
This must be updated for easier usage`,
	Run: func(cmd *cobra.Command, args []string) {
		switch side(orderSide) {
		case buy:
			if err := buyOrder(viper.GetString("token"), orderISIN, orderPrice, orderQuantity, orderValidityDate); err != nil {
				logrus.Errorf("order failed: %v", err)
			}
			return
		case sell:
			if err := sellOrder(viper.GetString("token"), orderISIN, orderPrice, orderQuantity, orderValidityDate); err != nil {
				logrus.Errorf("order failed: %v", err)
			}
		default:
			logrus.Error("invalid value for side")
			return
		}
	},
}

var orderAtCmd = &cobra.Command{
	Use:   "orderAt",
	Short: "low level command for placing orders at specific time",
	Long: `low-level command for placing orders at a specific time.
This must be updated for easier usage`,
	Run: func(cmd *cobra.Command, args []string) {
		var (
			err         error
			timeStart   time.Time
			timeFinish  time.Time
			endTicker   *time.Ticker
			startTicker *time.Ticker
		)
		timeStart, err = time.Parse(time.RFC3339Nano, orderAt)
		if err != nil {
			logrus.Errorf("invalid time format: %v", err)
			return
		}

		startTicker = time.NewTicker(time.Until(timeStart))
		if orderAggressive {
			timeFinish = timeStart.Add(3 * time.Second)
			endTicker = time.NewTicker(time.Until(timeFinish))
			timeStart = timeStart.Add(-3 * time.Second)
			startTicker = time.NewTicker(time.Until(timeStart)) //re-create start ticker
		}

		switch side(orderSide) {
		case buy:
			if !orderAggressive {
				select {
				case <-startTicker.C:
					for i, s := range orderISINSlice {
						if err := buyOrder(viper.GetString("token"), s, orderPriceSlice[i], orderQuantitySlice[i], orderValidityDate); err != nil {
							logrus.Errorf("order failed: %v", err)
						}
					}
					return
				}
			}

			<-startTicker.C
			for {
				select {
				case <-endTicker.C:
					return
				default:
					for i, s := range orderISINSlice {
						if err := buyOrder(viper.GetString("token"), s, orderPriceSlice[i], orderQuantitySlice[i], orderValidityDate); err != nil {
							logrus.Errorf("order failed: %v", err)
						}
					}
				}
			}
		case sell:
			if !orderAggressive {
				select {
				case <-startTicker.C:
					if err := sellOrder(viper.GetString("token"), orderISIN, orderPrice, orderQuantity, orderValidityDate); err != nil {
						logrus.Errorf("order failed: %v", err)
					}
					return
				}
			}

			<-startTicker.C
			for {
				select {
				case <-endTicker.C:
					return
				default:
					if err := sellOrder(viper.GetString("token"), orderISIN, orderPrice, orderQuantity, orderValidityDate); err != nil {
						logrus.Errorf("order failed: %v", err)
					}
				}
			}
		default:
			logrus.Error("invalid value for side")
			return
		}
	},
}

// orderBody is sent as a JSON POST request to https://d11.emofid.com/easy/api/OmsOrder
type orderBody struct {
	EasySource         int    `json:"easySource"` // usually 1?
	FinanceID          int    `json:"financeId"`  // usually 1?
	ISIN               string `json:"isin"`       // IRO1PTEH0001
	Price              uint   `json:"price"`
	Quantity           uint   `json:"quantity"`
	ReferenceKey       string `json:"referenceKey"`       // uuid V4
	Side               int    `json:"side"`               // 0: Buy, 1: Sell
	ValidityDateJalali string `json:"validityDateJalali"` // usually time.Now() Format: YYYY/MM/DD
	ValidityType       int    `json:"validityType"`       // 74?
}

// stockOrderData is received from https://d11.emofid.com/easy/api/MarketData/GetSymbolDetailsData/{ISIN:string}/StockOrderData
type stockOrderData struct {
	AskPrice         uint64 `json:"askPrice"`
	BidPrice         uint64 `json:"bidPrice"`
	HighAllowedPrice uint64 `json:"highAllowedPrice"`
	LowAllowedPrice  uint64 `json:"lowAllowedPrice"`
	MaxQuantityOrder uint64 `json:"maxQuantityOrder"`
	MinQuantityOrder uint64 `json:"minQuantityOrder"`
}

func buyOrder(token string, isin string, price uint, quantity uint, validityDate string) error {
	sendOrder(token, isin, price, quantity, buy, validityDate)
	return nil
}

func sellOrder(token string, isin string, price uint, quantity uint, validityDate string) error {
	sendOrder(token, isin, price, quantity, sell, validityDate)
	return nil
}

func sendOrder(token string, isin string, price uint, quantity uint, orderSide side, validityDate string) {
	referenceKey := uuid.Must(uuid.NewV4()).String()
	reqBody := orderBody{
		EasySource:         1,
		FinanceID:          1,
		ISIN:               isin,
		Price:              price,
		Quantity:           quantity,
		ReferenceKey:       referenceKey,
		Side:               int(orderSide),
		ValidityDateJalali: validityDate,
		ValidityType:       74,
	}

	v, _ := json.Marshal(&reqBody)
	d := utils.CustomCollector()
	d.OnRequest(func(r *colly.Request) {
		r.Headers.Set("Content-Type", "application/json")
		r.Headers.Set("Accept", "application/json, text/plain, */*")
		r.Headers.Set("Authorization", "Bearer "+token)
	})
	d.OnResponse(func(r *colly.Response) {
		logrus.Info(isin, string(r.Body))
	})
	d.OnError(func(r *colly.Response, err error) {
		logrus.Errorf("send order failed: %v", err)
	})
	_ = d.PostRaw("https://d11.emofid.com/easy/api/OmsOrder", v)
}

func init() {
	rootCmd.AddCommand(orderCmd, orderAtCmd)
	orderCmd.Flags().SortFlags = false
	orderAtCmd.Flags().SortFlags = false

	orderCmd.Flags().IntVarP(&orderSide, "side", "s", 0, "order type. 0:Buy 1:Sel")
	_ = orderCmd.MarkFlagRequired("side")
	orderCmd.Flags().StringVarP(&orderISIN, "isin", "i", "", "isin of the stock")
	_ = orderCmd.MarkFlagRequired("isin")
	orderCmd.Flags().UintVarP(&orderPrice, "price", "p", 0, "price of the order")
	_ = orderCmd.MarkFlagRequired("price")
	orderCmd.Flags().UintVarP(&orderQuantity, "quantity", "q", 0, "order quantity")
	_ = orderCmd.MarkFlagRequired("quantity")
	orderCmd.Flags().StringVarP(&orderValidityDate, "validity", "v", "", "order validity date")
	_ = orderCmd.MarkFlagRequired("validity")

	orderAtCmd.Flags().IntVarP(&orderSide, "side", "s", 0, "order type. 0:Buy 1:Sel")
	_ = orderAtCmd.MarkFlagRequired("side")
	orderAtCmd.Flags().StringSliceVarP(&orderISINSlice, "isin", "i", nil, "isin of the stock")
	_ = orderAtCmd.MarkFlagRequired("isin")
	orderAtCmd.Flags().UintSliceVarP(&orderPriceSlice, "price", "p", nil, "price of the order")
	_ = orderAtCmd.MarkFlagRequired("price")
	orderAtCmd.Flags().UintSliceVarP(&orderQuantitySlice, "quantity", "q", nil, "order quantity")
	_ = orderAtCmd.MarkFlagRequired("quantity")
	orderAtCmd.Flags().StringVarP(&orderValidityDate, "validity", "v", "", "order validity date")
	_ = orderAtCmd.MarkFlagRequired("validity")
	orderAtCmd.Flags().StringVar(&orderAt, "at", "", "send order at specified time '2006-01-02T15:04:05.999999999Z07:00'")
	_ = orderAtCmd.MarkFlagRequired("at")
	orderAtCmd.Flags().BoolVarP(&orderAggressive, "aggressive", "a", false, "start sending order at specified time and don't stop. this commands runs for 6 seconds")
	_ = orderAtCmd.MarkFlagRequired("aggressive")
}
