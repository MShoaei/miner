package cmd

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/MShoaei/Stock/miner/models"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

// crawlCmd represents the crawl command
var crawlCmd = &cobra.Command{
	Use:   "crawl",
	Short: "crawl does various data mining tasks",
	Long: `crawl does various data mining tasks.
for the sub-commands to work properly a postgres database is required.
A file named dbconfig.yaml should also exist which contains
the database's host, port, username, password, dbname, etc`,
}

// crawlCmd represents the crawl command
var crawlSymbolsCmd = &cobra.Command{
	Use:   "symbols",
	Short: "update sympols in the database",
	Long: `extracts the symbols from html extracted from http://www.tsetmc.com/Loader.aspx?ParTree=15131F
and saves them in the database`,
	Run: func(cmd *cobra.Command, args []string) {
		fName, err := cmd.Flags().GetString("file")
		if err != nil {
			logrus.Fatalf("failed to get --file argument: %v", err)
		}

		file, err := os.Open(fName)
		if err != nil {
			logrus.Fatalf("failed to open html file: %v", err)
		}

		db, err := getDB()
		if err != nil {
			logrus.Fatalf("connection failed: %v", err)
		}

		dom, err := goquery.NewDocumentFromReader(file)
		if err != nil {
			logrus.Fatalf("failed to create dom from file: %v", err)
		}

		tx, err := db.BeginTx(context.Background(), nil)
		if err != nil {
			logrus.Fatalf("failed to create a new transaction: %v", err)
		}

		dom.Find("#main>div[id]").Each(func(i int, s *goquery.Selection) {
			// logrus.Infoln(i)
			symbol := models.Symbol{}
			digit, exists := s.Attr("id")
			if !exists {
				logrus.Warningln("div tag without id attribute")
				return
			}
			symbol.TseDigit = digit
			exists, err := models.Symbols(qm.Where("tse_digit=?", digit)).Exists(context.Background(), db)
			if err != nil {
				logrus.Warningf("failed to check if symbol already exists: %v", err)
				return
			}

			if exists {
				logrus.Infof("symbol with TSEDigit \"%s\" already exists", digit)
				return
			}
			resp, err := http.Get(fmt.Sprintf("http://www.tsetmc.com/loader.aspx?ParTree=151311&i=%s", digit))
			if err != nil {
				logrus.Fatalf("http connection failed: %v", err)
			}
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				logrus.Fatalf("failed to read body: %v", err)
			}

			index := bytes.Index(body, []byte("InstrumentID='")) + 14
			symbol.Isin = string(body[index : index+12])
			symbol.Name = s.Find("div:first-child").Text()
			logrus.Debugln(symbol)
			err = symbol.Insert(context.Background(), tx, boil.Infer())
			if err != nil {
				logrus.Warningf("failed to insert symbol with ISIN:%s to transaction: %v", symbol.Isin, err)
			}
		})

		if err := tx.Commit(); err != nil {
			logrus.Fatalf("failed to to commit symbols: %v", err)
		}

		count, err := models.Symbols().Count(context.Background(), db)
		if err != nil {
			logrus.Fatalf("count failed: %v", err)
		}
		logrus.Infof("table symbols has %d rows", count)
	},
}

var crawlDailyRecordsCmd = &cobra.Command{
	Use:   "daily-records",
	Short: "extract daily records",
	Long: `extract daily records of a symbol by downloading 
the CSV report and parsing it`,
	Run: func(cmd *cobra.Command, args []string) {
		const threads = 8
		db, err := getDB()
		if err != nil {
			logrus.Fatalf("connection failed: %v", err)
		}
		allSymbols, err := models.Symbols().All(context.Background(), db)
		if err != nil {
			logrus.Fatalf("failed to get Symbols table: %v", err)
		}

		wokersChan := createWorker(threads)
		for _, s := range allSymbols {
			w := <-wokersChan
			go w.getDailyData(db, s)
		}

		// this ensures all jobs are finished
		for i := 0; i < threads; i++ {
			<-wokersChan
		}
	},
}

type worker struct {
	id   int
	done chan<- *worker
}

func createWorker(count int) chan *worker {
	ch := make(chan *worker, count)
	for i := 0; i < count; i++ {
		ch <- &worker{id: i, done: ch}
	}
	return ch
}

func (w *worker) getDailyData(db *sql.DB, symbol *models.Symbol) {
	defer func() {
		w.done <- w
	}()

	const csvURL = "http://www.tsetmc.com/tsev2/data/Export-txt.aspx?t=i&a=1&b=0&i="
	resp, err := http.Get(csvURL + symbol.TseDigit)
	if err != nil {
		logrus.Warningf("failed to get csv file: %v", err)
		return
	}
	defer resp.Body.Close()

	dailyDataReader := csv.NewReader(resp.Body)
	dailyDataReader.FieldsPerRecord = 12
	records, err := dailyDataReader.ReadAll()
	if err != nil {
		logrus.Warningf("read csv file failed: %v", err)
		return
	}

	tx, err := db.BeginTx(context.Background(), nil)
	if err != nil {
		logrus.Warningf("failed to create a new transaction: %v", err)
		return
	}
	for _, r := range records[1:] {
		date, _ := time.Parse("20060102", r[1])
		exists, err := models.DailyRecords(
			qm.Where(models.DailyRecordColumns.SymbolID+"=?", symbol.ID),
			qm.Where("date=?", date),
		).Exists(context.Background(), db)
		if err != nil {
			logrus.Warningf("failed to check if daily record already exists: %v", err)
			continue
		}
		if exists {
			logrus.Infof("worker ID: %d, daily record for symbol \"%s\" on date \"%s\" already exists. skipping symbol...", w.id, symbol.Isin, date)
			tx.Commit()
			return
		}

		dailyRecord := models.DailyRecord{}
		dailyRecord.SymbolID = symbol.ID
		dailyRecord.Date = date

		open, _ := strconv.Atoi(r[2][:len(r[2])-3])
		dailyRecord.Open = open

		high, _ := strconv.Atoi(r[3][:len(r[3])-3])
		dailyRecord.MaxPrice = high

		low, _ := strconv.Atoi(r[4][:len(r[4])-3])
		dailyRecord.MinPrice = low

		close, _ := strconv.Atoi(r[5][:len(r[5])-3])
		dailyRecord.Close = close

		value, _ := strconv.ParseInt(r[6], 10, 64)
		dailyRecord.Value = value

		volume, _ := strconv.ParseInt(r[7], 10, 64)
		dailyRecord.Volume = volume

		quantity, _ := strconv.Atoi(r[8])
		dailyRecord.Quantity = quantity

		finalDealPrice, _ := strconv.Atoi(r[11][:len(r[11])-3])
		dailyRecord.FinalDealPrice = finalDealPrice

		if err := dailyRecord.Insert(context.Background(), tx, boil.Infer()); err != nil {
			logrus.Warningf("failed to insert record %v: %v", dailyRecord, err)
			continue
		}
		logrus.Tracef("worker ID: %d, record added %v", w.id, dailyRecord)
	}
	if err := tx.Commit(); err != nil {
		logrus.Fatalf("failed to to commit symbols: %v", err)
	}
	logrus.Infof("worker ID: %d, all record for %s added", w.id, symbol.Isin)
}

func getDB() (*sql.DB, error) {
	dialect := os.Getenv("DB_DIALECT")
	if dialect == "" {
		dialect = "postgres"
	}
	connString := os.Getenv("DB_DATASOURCE")
	if connString != "" {
		return sql.Open(dialect, os.ExpandEnv(connString))
	}
	connString = os.ExpandEnv("host=${DB_HOST} dbname=${PG_DB} user=${DB_USER} password=${PG_PASSWORD} sslmode=${DB_SSLMODE}")
	return sql.Open(dialect, connString)

}

func init() {
	rootCmd.AddCommand(crawlCmd)
	crawlCmd.AddCommand(crawlSymbolsCmd, crawlDailyRecordsCmd)

	// crawlCmd.PersistentFlags().BoolVar(&isTest, "test", false, "set this if the database should be re-created")

	crawlSymbolsCmd.Flags().String("file", "", "html file to parse and extract data from")
	_ = crawlSymbolsCmd.MarkFlagRequired("file")
}
