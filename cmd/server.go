package cmd

import (
	"github.com/go-cmd/cmd"
	"github.com/kataras/iris/v12"
	"github.com/spf13/cobra"
	"os"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Launch server on port $PORT",
	Long: `Launch server on port $PORT.
this server executes all commands received via json and returns the result`,
	Run: func(cmd *cobra.Command, args []string) {
		app := iris.Default()
		app.Post("/", commandHandler)
		port := os.Getenv("PORT")
		if port == "" {
			port = "80"
		}
		app.Run(iris.Addr(":" + port))
	},
}

func commandHandler(c iris.Context) {
	data := struct {
		Cmd []string `json:"cmd"`
	}{}
	c.ReadJSON(&data)
	command := cmd.NewCmd(data.Cmd[0], data.Cmd[1:]...)
	result := <-command.Start()
	c.StatusCode(iris.StatusOK)
	c.JSON(iris.Map{
		"result": result,
	})
}

func init() {
	rootCmd.AddCommand(serverCmd)
}
