package cmd

import (
	"log"
	"strings"

	"bitbucket.org/MShoaei/Stock/miner/utils"
	"github.com/gocolly/colly/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "login using credentials in .StockConfig.yaml",
	Long: `Attempts to login and saves the JWT token in .StockConfig.yaml for later use.
this never stops unless it succeeds.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		var token string
		var err error

		if aggressive, _ := cmd.Flags().GetBool("aggressive"); aggressive {
			token = mustLogin(viper.GetString("username"), viper.GetString("password"))
			viper.Set("token", token)
			return nil
		}
		token, err = login(viper.GetString("username"), viper.GetString("password"))
		if err != nil {
			return err
		}
		viper.Set("token", token)
		return nil
	},
}

// mustLogin returns a JWT token without "Bearer" prefix. It won't stop unless it succeeds
func mustLogin(username, password string) (accessToken string) {
	var requestToken string
	returnURL := getReturnURL()

	verificationExtractor := utils.CustomCollector()
	verificationExtractor.OnHTML(`input[name="__RequestVerificationToken"]`, func(e *colly.HTMLElement) {
		requestToken = e.Attr("value")
	})
	verificationExtractor.Visit("http://my.emofid.com/")

	d := verificationExtractor.Clone()
	d.OnScraped(func(r *colly.Response) {
		all := strings.Split(r.Request.URL.Fragment, "&")
		for _, v := range all {
			if !strings.HasPrefix(v, "access_token") {
				continue
			}
			accessToken = strings.Split(v, "=")[1]
			log.Printf("successfully logged in. JWT: %s", accessToken)
			return
		}
	})
	d.OnError(func(r *colly.Response, err error) {
		log.Printf("login failed: %d", r.StatusCode)
		log.Printf("login error: %v", err)
		log.Println("retrying...")
		d.Post("https://account.emofid.com/Login?returnUrl="+returnURL, map[string]string{
			"ReturnUrl":                  returnURL,
			"Username":                   username,
			"Password":                   password,
			"button":                     "login",
			"__RequestVerificationToken": requestToken,
			"RememberLogin":              "false",
		})
	})
	d.Post("https://account.emofid.com/Login?returnUrl="+returnURL, map[string]string{
		"ReturnUrl":                  returnURL,
		"Username":                   username,
		"Password":                   password,
		"button":                     "login",
		"__RequestVerificationToken": requestToken,
		"RememberLogin":              "false",
	})
	return accessToken
}

func login(username, password string) (accessToken string, err error) {
	var requestToken string
	returnURL := getReturnURL()

	verificationExtractor := utils.CustomCollector()
	verificationExtractor.OnHTML(`input[name="__RequestVerificationToken"]`, func(e *colly.HTMLElement) {
		requestToken = e.Attr("value")
	})
	verificationExtractor.Visit("http://my.emofid.com/")

	d := verificationExtractor.Clone()
	d.OnScraped(func(r *colly.Response) {
		all := strings.Split(r.Request.URL.Fragment, "&")
		for _, v := range all {
			if !strings.HasPrefix(v, "access_token") {
				continue
			}
			accessToken = strings.Split(v, "=")[1]
			log.Printf("successfully logged in. JWT: %s", accessToken)
			return
		}
	})
	d.OnError(func(r *colly.Response, e error) {
		log.Printf("login failed: %d", r.StatusCode)
		log.Printf("login error: %v", e)
		err = e
	})
	d.Post("https://account.emofid.com/Login?returnUrl="+returnURL, map[string]string{
		"ReturnUrl":                  returnURL,
		"Username":                   username,
		"Password":                   password,
		"button":                     "login",
		"__RequestVerificationToken": requestToken,
		"RememberLogin":              "false",
	})
	return accessToken, err
}

func getReturnURL() string {
	returnURL := "/connect/authorize/callback?client_id=easy2_client&redirect_uri=https%3A%2F%2Fd.easytrader.emofid.com%2Fauth-callback&response_type=token%20id_token&scope=easy2_api%20openid&state=4c60f95ce98d45cca55aa5368909da63&nonce=ee828341121f424e94341dc7a646e4a4"

	return returnURL
}

func init() {
	rootCmd.AddCommand(loginCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// loginCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	loginCmd.Flags().BoolP("aggressive", "a", false, "never stop unless login is successful")
}
