package main

//go:generate sqlboiler --wipe psql

import (
	"bitbucket.org/MShoaei/Stock/miner/cmd"
	_ "github.com/lib/pq"
)

func main() {
	cmd.Execute()
}
