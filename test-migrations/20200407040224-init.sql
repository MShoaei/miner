-- +migrate Up
CREATE TABLE symbols (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL UNIQUE,
    isin VARCHAR(100) NOT NULL UNIQUE,
    tse_digit VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE daily_records (
    id SERIAL PRIMARY KEY,
    symbol_id INTEGER NOT NULL REFERENCES symbols(id),
    date DATE NOT NULL,
    quantity INTEGER NOT NULL,
    volume BIGINT NOT NULL,
    value BIGINT NOT NULL,
    open INTEGER NOT NULL,
    final_deal_price INTEGER NOT NULL,
    close INTEGER NOT NULL,
    min_price INTEGER NOT NULL,
    max_price INTEGER NOT NULL
);

CREATE TABLE detail_records (
    id SERIAL PRIMARY KEY,
    symbol_id INTEGER NOT NULL REFERENCES symbols(id),
    daily_record_id INTEGER NOT NULL REFERENCES daily_records(id),
    time TIME WITH TIME ZONE NOT NULL,
    volume BIGINT NOT NULL,
    price INTEGER NOT NULL
);

-- +migrate Down
DROP TABLE detail_records;
DROP TABLE daily_records;
DROP TABLE symbols;