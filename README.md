# DataMiner

Extracts Irans's stock market data for further processing.

## What you need

files needed for the docker-compose to work are:

- StockConfig.yaml: login credentials for [easy trader]("https://d.easytrader.emofid.com")
- .env: should be something like below

        DB_DIALECT=postgres
        DB_DATASOURCE=host=${DB_HOST} port=${DB_PORT} dbname=${PG_DB} user=${DB_USER} password=${PG_PASSWORD} sslmode=${DB_SSLMODE}
        DB_HOST=db
        DB_PORT=5432
        PG_DB=stock
        DB_USER=postgres
        PG_PASSWORD=testdbpassword
        DB_SSLMODE=disable
---
code for migrations:

```bash
go get -v github.com/rubenv/sql-migrate/...

DB_HOST=localhost DB_PORT=5432 PG_DB=stock DB_USER=postgres PG_PASSWORD=testdbpassword DB_SSLMODE=disable sql-migrate up
```

build app with:

```bash
go build -o miner .

$ ./miner

This is a CLI for easier and faster ordering in Iran's stock market'

Usage:
  miner [command]

Available Commands:
  crawl       crawl does various data mining tasks
  help        Help about any command
  login       login using credentials in .StockConfig.yaml
  order       low level command for placing orders
  orderAt     low level command for placing orders at specific time
  server      Launch server on port $PORT

Flags:
      --config string   config file (default is $HOME/.StockConfig.yaml)
      --debug           set log level to Trace
  -h, --help            help for miner

Use "miner [command] --help" for more information about a command.
```

to populate symbols table, export symmbols as html file from http://www.tsetmc.com/Loader.aspx?ParTree=15131F and run:
```bash
DB_HOST=localhost DB_PORT=5432 PG_DB=stock_test DB_USER=postgres PG_PASSWORD=testdbpassword DB_SSLMODE=disable ./miner crawl symbols --debug --file <html file>
```

to get daily records for each symbol in symbols table, run:
```bash
DB_HOST=localhost DB_PORT=5432 PG_DB=stock_test DB_USER=postgres PG_PASSWORD=testdbpassword DB_SSLMODE=disable ./miner crawl daily-records --debug
```